from django.db import models
from django.contrib.auth.forms import UserCreationForm
# Create your models here.
class AccountCreationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'placeholder':'  Enter username...'})
        self.fields['email'].widget.attrs.update({'placeholder':'  Enter email...'})
        self.fields['password1'].widget.attrs.update({'placeholder':'  Enter password...'})        
        self.fields['password2'].widget.attrs.update({'placeholder':'  Repeat password'})