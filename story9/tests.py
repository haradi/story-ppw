from django.test import TestCase, Client
from django.urls import resolve
# Create your tests here.

class Story9Test(TestCase):
    def test_url_index_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_index_using_index_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9/index.html')

    def test_url_register_exist(self):
        response = Client().get('/story9/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_register_using_register_template(self):
        response = Client().get('/story9/register/')
        self.assertTemplateUsed(response, 'story9/register.html')

    def test_url_login_exist(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_login_using_login_template(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'story9/login.html')
    
    def test_register_success(self):
        register_form = {
            'username' : 'zul',
            'email' : 'zul04@gmail.com',
            'password1' : 'passwordzul',
            'password2' : 'passwordzul',
        }

        response = Client().post('/story9/register/', register_form, follow=True)
        self.assertRedirects(response, '/story9/login/', status_code=302, target_status_code=200, fetch_redirect_response=True)

        html_response = response.content.decode('utf8')
        self.assertIn("Akun dengan nama zul berhasil dibuat", html_response)
    
    def test_register_failed(self):
        register_form = {
            'username' : 'zul',
            'email' : 'zul04@gmail.com',
            'password1' : 'passwordzul',
            'password2' : 'passwordbeda',
        }

        response = Client().post('/story9/register/', register_form)

        html_response = response.content.decode('utf8')
        self.assertIn('<li>Password confirmation: The two password fields didn’t match.</li>', html_response)
    
    def test_login_content_exists(self):
        response = Client().get('/story9/login/')

        html_response = response.content.decode('utf8')
        self.assertIn("LOGIN", html_response)
        self.assertIn("Username...", html_response)
        self.assertIn("Password...", html_response)
    
    def test_login_success(self):
        register_form = {
            'username' : 'zul',
            'email' : 'zulfahri123@gmail.com',
            'password1' : 'correctpass123',
            'password2' : 'correctpass123',
        }

        Client().post('/story9/register/', register_form)

        login_form = {
            'username' : 'zul',
            'password' : 'correctpass123',
        }

        response = Client().post('/story9/login/', login_form, follow=True)
        self.assertRedirects(response, '/story9/', status_code=302, target_status_code=200, fetch_redirect_response=True)

        html_response = response.content.decode('utf8')
        self.assertIn("Halo zul", html_response)
    
    def test_login_failed(self):
        register_form = {
            'username' : 'zul',
            'email' : 'zulfahri123@gmail.com',
            'password1' : 'matchedpassword123',
            'password2' : 'matchedpassword123',
        }

        Client().post('/story9/register/', register_form)

        login_form = {
            'username' : 'zul',
            'password' : 'wrongpassword',
        }

        response = Client().post('/story9/login/', login_form, follow=True)
        self.assertEquals(response.status_code, 200)

        html_response = response.content.decode('utf8')
        self.assertIn("Username atau password salah", html_response)
    
    def test_logout(self):
        register_form = {
            'username' : 'zul',
            'email' : 'zulfahri123@gmail.com',
            'password1' : 'correctpassword123',
            'password2' : 'correctpassword123',
        }

        Client().post('/story9/register/', register_form)

        login_form = {
            'username' : 'zul',
            'password' : 'correctpassword123',
        }

        Client().post('/story9/login/', login_form, follow=True)
        Client().post('/story9/logout/')

        response = Client().get('/story9/')
        html_response = response.content.decode('utf8')
        self.assertIn("Hello stranger", html_response)