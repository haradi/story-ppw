from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from datetime import datetime
import json

# Create your views here.
def index(request):
    return render(request, 'story9/index.html')

def registerPage(request):
    form = CreateUserForm()

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Akun dengan nama ' + form.cleaned_data.get('username') + ' berhasil dibuat')
            return redirect('story9:login')
    else:
        form = CreateUserForm()
    

    return render(request, 'story9/register.html', {'form' : form})

def loginPage(request):
    context = {}
    
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            context = compileInformation(user)
            for key, value in context.items():
                request.session[key] = value
            return redirect('story9:index')
        else:
            messages.info(request, 'Username atau password salah')
    
    return render(request, 'story9/login.html', context)

def logOut(request):
    logout(request)
    return redirect('story9:login')

def compileInformation(user):
    temp = dict()
    if user.first_name == "":
        temp['first_name'] = "-"
    if user.last_name == "":
        temp['last_name'] = "-"
    if user.is_staff:
        temp['peran'] = "Staff"
    else:
        temp['peran'] = "User"
    date_joined = str(user.date_joined).split(".")[0]
    last_login = str(user.last_login).split(".")[0]
    date_joined = datetime.strptime(date_joined, "%Y-%m-%d %H:%M:%S")
    last_login = datetime.strptime(last_login, "%Y-%m-%d %H:%M:%S")
    temp['date_joined'] = str(date_joined)
    temp['last_login'] = str(last_login)
    return temp
    