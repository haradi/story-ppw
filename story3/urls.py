from django.urls import path

from . import views

app_name = 'story3'

urlpatterns =[
    path('', views.homepage, name='homepage'),
    path('homepage/', views.homepage, name='homepage'),
    path('blogs/', views.blogs, name='blogs'),
    path('about_me/', views.about_me, name="about_me"),
    path('blogs/agustus17th/', views.agustus17th, name='blogs/agustus17th'),
    path('liat_matkul/', views.liat_matkul, name='liat_matkul'),
    path('tambah_matkul/', views.create_matkul, name="tambah_matkul"),
    path('matkul/<pk>/', views.matkul, name="matkul"),
    path('hapus_matkul/<pk>/', views.delete_matkul, name="hapus_matkul")
]