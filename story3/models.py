from django.db import models

# Create your models here.
class Matkul(models.Model): 
    nama = models.CharField(max_length=30, null=False, blank=False)
    pengajar = models.CharField(max_length=30, null=False, blank=False)
    sks = models.CharField(max_length=1, null=False, blank=False)
    deskripsi = models.TextField(null=False, blank=False)
    semester = models.CharField(max_length=30, null=False, blank=False)
    ruang = models.CharField(max_length=30, null=False, blank=False)
    
    def __str__(self):
        return self.nama
