from django import forms
from django.forms import ModelForm
from .models import Matkul
from django.core.exceptions import ValidationError

class MatkulForm(ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'nama',
            'pengajar',
            'sks',
            'semester',
            'ruang',
            'deskripsi',
        ]
        widgets = {
            'nama' : forms.TextInput(attrs={
                'class' : 'form-control w-50',
                'placeholder' : 'Nama mata kuliah',
                'required' : 'true'}),
            'pengajar' : forms.TextInput(attrs={
                'class' : 'form-control w-50',
                'placeholder' : 'Nama dosen pengampu',
                'required' : 'true'}),
            'sks' : forms.TextInput(attrs={
                'class' : 'form-control w-50',
                'placeholder' : 'Jumlah SKS',
                'required' : 'true'}),
            'semester' : forms.TextInput(attrs={
                'class' : 'form-control w-50',
                'placeholder' : 'Term',
                'required' : 'true'}),
            'deskripsi' : forms.Textarea(attrs={
                'class' : 'form-control w-50',
                'placeholder' : 'Deskripsi mata kuliah',
                'required' : 'true'}),
            'ruang' : forms.TextInput(attrs={
                'class' : 'form-control w-50',
                'placeholder' : 'Ruang kuliah',
                'required' : 'true'}),
        }

    def clean_sks(self):
        sks = self.cleaned_data.get("sks")
        if not sks.isdigit():
            raise forms.ValidationError(sks +" is not a number, please insert a number for this field")
        return sks
    
    def clean_semester(self):
        semester = self.cleaned_data.get("semester")
        if not semester.isdigit():
            raise forms.ValidationError(semester + " is not a number, please insert a number for this field")
        return semester