from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import MatkulForm
from .models import Matkul

# Create your views here.
def homepage(request):
    return render(request, 'story3/homepage.html')

def blogs(request):
    return render(request, 'story3/blogs.html')

def about_me(request):
    return render(request, 'story3/profile.html')

def agustus17th(request):
    return render(request, 'story3/17-agustusan.html')

def liat_matkul(request):
    matkul = Matkul.objects.all()
    return render(request, 'story3/liat-matkul.html', {'matkuls' : matkul})

def create_matkul(request):
    form = MatkulForm()
    content = {'form' : form}

    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story3:liat_matkul')
        else :
            form = MatkulForm()

    return render(request, 'story3/matkul-form.html', content)

def matkul(request, pk):
    matkul = Matkul.objects.get(id=pk)
    content = {'matkul' : matkul}
    return render(request, 'story3/matkul-detail.html', content)

def delete_matkul(request, pk):
    matkul = Matkul.objects.get(id=pk)
    
    if request.method == 'POST':
        matkul.delete()
        return redirect('story3:liat_matkul')

    content = {'matkul':matkul}
    return render(request, 'story3/matkul-delete.html', content)