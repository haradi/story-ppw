from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpResponsePermanentRedirect
from .views import index, data
import json
# Create your tests here.

class Story8Test(TestCase):
    def test_url_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_index_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/index.html')
    
    def test_using_index_func(self):
        found = resolve('/story8/')
        self.assertAlmostEqual(found.func, index)

    def test_using_data_func(self):
        found = resolve('/story8/data/')
        self.assertAlmostEqual(found.func, data)
    
    def test_url_exist(self):
        response = Client().get('/story8/detail/<pk>/')
        self.assertEqual(response.status_code, 200)
    
    def test_content_exact_match(self):
        buku_id = "rfLsDwAAQBAJ"
        response = Client().get('/story8/detail/' + buku_id)
        html_response = response.content.decode('utf8')
        print(response)
