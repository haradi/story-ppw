from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
import json, requests
# Create your views here.
def index(request):
    return render(request, 'story8/index.html')

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    content = requests.get(url).content
    data = json.loads(content)
    return JsonResponse(data, safe=False)

def detail(request, pk):
    url = "https://www.googleapis.com/books/v1/volumes/" + pk
    content = json.loads(requests.get(url).content)
    #print(content)
    return render(request, 'story8/detail.html', {'buku' : content})