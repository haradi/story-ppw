$(document).ready(function(){
    $("#searchKeyword").keyup(function(){
        var input = $("#searchKeyword").val();
        // console.log(input);
        $.ajax({
            url:'/story8/data?q=' + input,
            success:function(data){
                var array_buku = data.items;
                $('#daftar-buku').empty();
                for(i = 0; i < array_buku.length; i++){
                    if(array_buku[i].volumeInfo.hasOwnProperty('imageLinks')){
                        if(array_buku[i].volumeInfo.imageLinks.hasOwnProperty('smallThumbnail')){
                            var title = array_buku[i].volumeInfo.title;
                            var categories = "";
                            var authors = "";
                            if(array_buku[i].volumeInfo.authors !== undefined){
                                for(j = 0; j < array_buku[i].volumeInfo.authors.length ; j++){
                                    authors += array_buku[i].volumeInfo.authors[j] + ", ";
                                }
                                authors = authors.slice(0, -2);
                            } else {
                                authors = "Author tidak ditemukan";
                            }
        
                            if(array_buku[i].volumeInfo.categories !== undefined){
                                for(j = 0; j < array_buku[i].volumeInfo.categories.length ; j++){
                                    categories += array_buku[i].volumeInfo.categories[j] + ", ";
                                }
                                categories = categories.slice(0, -2);
                            } else {
                                categories = "Kategori tidak ditemukan";
                            }
        
                            var thumbnail = array_buku[i].volumeInfo.imageLinks.thumbnail;
                            var id = array_buku[i].id;
                            var link = "<a href=\"detail\\".concat(array_buku[i].id);
                            link = link.concat("\"\\>");
                            title = link + title + "</a>";
                            $('#daftar-buku').append(
                                "<tr><td rowspan='3' colspan='2' class='thumbnail'><center><img src=" 
                                + thumbnail
                                + "></center></td><td>"
                                + title
                                + "</td></tr><tr><td>"
                                + categories
                                + "</td></tr><tr><td>"
                                + authors
                                + "</td></tr>"
                            );
                        }
                    }
                }
            }
        });
    });
});