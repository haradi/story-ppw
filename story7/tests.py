from django.test import TestCase, Client
from django.urls import resolve
from .views import index

class Story7Test(TestCase):
    def test_url_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/index.html')
    
    def test_using_index_func(self):
        found = resolve('/story7/')
        self.assertAlmostEqual(found.func, index)
    
    def test_text_in_html(self):
        response = Client().get('/story7/')
        html_response = response.content.decode('utf8')
        self.assertIn("Halo", html_response)
        self.assertIn("Selamat datang di Story 7 saya.", html_response)
        self.assertIn("Tentang saya", html_response)
        self.assertIn("Kegiatan saat ini", html_response)
        self.assertIn("Prestasi saya", html_response)
        self.assertIn("Kegiatan dan/atau organisasi", html_response)

