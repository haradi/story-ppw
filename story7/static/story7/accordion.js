
function hideAccordion(item){
    item.removeClass('active');
    item.find('.accordion-item').hide(500);
    
}

function showAccordion(item){
    item.addClass('active');
    item.find('.accordion-item').show(500);
}

function gsapanimation(){
    const tl =  gsap.timeline();
    tl.fromTo(
        $('.container-content'),
        1,
        {height:"0%"},
        {height:"85%", ease: Power2.easeInOut}
    ).fromTo(
        $('.container-content'),
        1.2,
        {
            width:"100%",
        },
        {
            width:"80%",
            ease: Power2.easeInOut
        }
    ).fromTo(
        $('.body-container'),
        1.2,
        {x:"-100%"},
        {x:"0%", ease: Power2.easeInOut},
        "-=1.2"
    ).fromTo(
        $('.pembuka'),
        1,
        {opacity:'0%'},
        {opacity:'100%', ease: Power2.easeInOut},
        '+=0.6'
    ).fromTo(
        $('.accordion'),
        1,
        {opacity: '0%'},
        {opacity:'100%', ease: Power1.easeInOut},
        '+=0.6'
    )
}

function heloTyped(){
    var typed = new Typed(
        '.list-tamu', {
            strings:[
                'Zul',
                'Kak Michelle',
                'Teman',
            ],
            typeSpeed:100,
            loop:true,
            backDelay:3000,
            showCursor:true,
            fadeOut:true
        }
    )
}

$(document).ready(function(){
    gsapanimation();
    setTimeout(heloTyped, 6000);
    $('.accordion-trigger').click(function(e){
        console.log($('.accordion-trigger').find('.position-changer'));
        var item = $(this).parents('.accordion');
        
        if (item.hasClass('active')){
            hideAccordion(item);
        }
        else{
            var other_item = $('.accordion').not(item);
            for(i = 0; i < other_item.length; i++){
                if($(other_item[i]).hasClass('active')){
                    hideAccordion($(other_item[i]));
                }
            }
            showAccordion(item);
        }
    }).find($('.position-changer')).click(function(e){
        return false;
    });
    $('.up').click(function(){
        var item = $(this).parents('.accordion');
        item.insertBefore(item.prev());
    });

    $('.down').click(function(){
        var item = $(this).parents('.accordion');
        item.insertAfter(item.next());
    });
});