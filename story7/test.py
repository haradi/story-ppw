from django.test import TestCase, Client
from django.urls import resolve
from .views import index

class Story7Test(TestCase):
    def test_url_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/index.html')
    
    def test_using_index_func(self):
        found = resolve('/story7/')
        self.assertAlmostEqual(found.func, index)